@Library('Jenkins-shared-libraries') _

pipeline {
    agent any

    environment {
        GIT_URL = 'git@gitlab.com:ahmed.sayed.ams/maven-calculator.git'
    }

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out the source code...'
                script {
                    sh "rm -rf maven-calculator"
                    withCredentials([sshUserPrivateKey(credentialsId: 'e5717187-6cff-474b-92f3-ef2a6c6109be', keyFileVariable: 'SSH_KEY')]) {
                        sh """
                            git config --global user.email "ahmed.sayed.ams@gmail.com"
                            git config --global user.name "ahmed sayed"
                            mkdir -p ~/.ssh
                            ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
                            ssh-agent bash -c 'ssh-add \$SSH_KEY; git clone \$GIT_URL'
                        """
                    }
                }
            }
        }

        stage('Cleanup') {
            steps {
                script {
                    dir("${env.WORKSPACE}") {
                        sh "rm -rf maven-calculator"
                    }
                }
            }
        }

        stage('Versioning') {
            steps {
                script {
                    echo 'Executing versioning logic from shared library...'
                    versionIncrement()
                }
            }
        }

        stage('Build') {
            steps {
                script {
                    echo 'Building the code...'
                    buildMaven()
                }
            }
        }

        stage('Pull & Commit Changes') {
            steps {
                withCredentials([sshUserPrivateKey(credentialsId: 'e5717187-6cff-474b-92f3-ef2a6c6109be', keyFileVariable: 'SSH_KEY')]) {
                    sh """
                        git config --global user.email "ahmed.sayed.ams@gmail.com"
                        git config --global user.name "ahmed sayed"
                        mkdir -p ~/.ssh
                        ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
                        ssh-agent bash -c 'ssh-add \$SSH_KEY; git pull --rebase origin master; git add .; git commit -m "Automated commit from Jenkins"'
                    """
                }
            }
        }

        stage('Push Changes') {
            steps {
                withCredentials([sshUserPrivateKey(credentialsId: 'e5717187-6cff-474b-92f3-ef2a6c6109be', keyFileVariable: 'SSH_KEY')]) {
                    script {
                        // Attempt to push changes, retrying with force push if necessary
                        def pushAttempt = sh(script: "git push origin master", returnStatus: true)
                        if (pushAttempt != 0) {
                            echo 'Initial push failed; likely due to the branch being behind. Attempting force push...'
                            sh "git push origin master --force"
                        }
                    }
                }
            }
        }
    } // End of stages block

    post {
        success {
            echo 'Build successful!'
        }
        failure {
            echo 'Build failed!'
        }
    }
}
